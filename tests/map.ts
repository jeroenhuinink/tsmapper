import { createMapper } from '../src/tsmapper';
import { Source } from '../src/types';

describe('map', () => {
  it('should apply map if defined', () => {
    const testMapper = createMapper().field('name', {
      map: (value: any, source: Source) => {
        return 'test';
      },
    });

    const expected = { name: 'test' };
    const actual = testMapper.map({ name: 'sentastic' });
    expect(actual).toEqual(expected);
  });

  it('should have access to other fields in source', () => {
    const testMapper = createMapper().field('name', {
      map: (value: any, source: Source) => {
        return source['other'];
      },
    });

    const expected = { name: 'sentastic' };
    const actual = testMapper.map({ name: 'other', other: 'sentastic' });
    expect(actual).toEqual(expected);
  });

  it('should be applied to default value', () => {
    const testMapper = createMapper().field('name', {
      default: 'other',
      map: (value: any, source: Source) => {
        return 'test';
      },
    });
    const expected = { name: 'test' };
    const actual = testMapper.map({ name: 'sentastic' });
    expect(actual).toEqual(expected);
  });

  it('should determine type,', () => {
    const testMapper = createMapper().field('name', {
      map: (value: any, source: Source) => {
        return 1;
      },
    });

    const expected = { name: 1 };
    const actual = testMapper.map({ name: '1' });
    expect(actual).toEqual(expected);
  });
});
