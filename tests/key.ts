import { createMapper } from '../src/tsmapper';

describe('key', () => {
  test('should use key to read an attribute', () => {
    const testMapper = createMapper().field('name', { key: 'test' });

    const expected = { name: 'sentastic' };
    const actual = testMapper.map({ test: 'sentastic' });
    expect(actual).toEqual(expected);
  });
});
