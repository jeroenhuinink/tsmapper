import { createMapper } from '../src';

describe('nested mapper', () => {
  describe('array', () => {
    it('should use mapper from field', () => {
      const aMapper = createMapper('a').field('value', { type: 'string' });
      const bMapper = createMapper('b').field('a', { key: 'c', mapper: aMapper });

      const actual = bMapper.map({ c: { value: 'test' } });
      const expected = { a: { value: 'test' } };

      expect(actual).toEqual(expected);
    });

    it('can be optional', () => {
      const aMapper = createMapper('a').field('value', { type: 'string' });
      const bMapper = createMapper('b')
        .field('a', { key: 'c', optional: true, itemType: { mapper: aMapper } })
        .field('d', { default: 'D' });

      const actual = bMapper.map({});
      const expected = { d: 'D' };

      expect(actual).toEqual(expected);
    });

    it('should throw if not present', () => {
      const aMapper = createMapper('AMapper').field('value', { type: 'string' });
      const bMapper = createMapper('TestMapper')
        .field('a', { key: 'c', optional: false, itemType: { mapper: aMapper } })
        .field('d', { default: 'D' });

      const actual = () => bMapper.map({});

      expect(actual).toThrowError('TestMapper: Property "c" not found in: {}');
    });

    it('should allow empty array for default', () => {
      const aMapper = createMapper('a').field('value', { type: 'string' });

      const bMapper = createMapper('b')
        .field('a', { itemType: { mapper: aMapper }, default: [] as { value: string }[] })
        .field('d', { default: 'D' });

      const actual = bMapper.map({});
      const expected = { d: 'D', a: [] };

      expect(actual).toEqual(expected);
    });

    it('should allow non-empty array for default', () => {
      const aMapper = createMapper('a').field('value', { type: 'string' });
      const bMapper = createMapper('b')
        .field('a', { key: 'c', default: [{ value: 'v' }], itemType: { mapper: aMapper } })
        .field('d', { default: 'D' });

      const actual = bMapper.map({});
      const expected = { d: 'D', a: [{ value: 'v' }] };

      expect(actual).toEqual(expected);
    });
  });

  describe('array of enums', () => {
    it('should allow non-empty array for default', () => {
      enum Enums {
        'A' = 'A',
        'B' = 'B',
      }

      function mapEnums(role: any): Enums {
        if (role in Enums) {
          return role as Enums;
        }
        throw new Error('unexpected value');
      }

      const aMapper = createMapper('a').field('value', { type: 'array', itemType: { type: 'enum', map: mapEnums } });
      type A = ReturnType<typeof aMapper.map>;

      const actual = aMapper.map({ value: ['A', 'B'] });
      const expected: A = { value: [Enums.A, Enums.B] };

      expect(actual).toEqual(expected);
    });
  });
});
