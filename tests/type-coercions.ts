import { createMapper } from '../src/tsmapper';

describe('type coercions', () => {
  test('should coerce number to string', () => {
    const testMapper = createMapper().field('name', { type: 'string' });

    const expected = { name: '1' };
    const actual = testMapper.map({ name: 1 });
    expect(actual).toEqual(expected);
  });

  test('should coerce string to number', () => {
    const testMapper = createMapper().field('name', { type: 'number' });

    const expected = { name: 1 };
    const actual = testMapper.map({ name: '1' });
    expect(actual).toEqual(expected);
  });

  test('should coerce string to Nan when not a number', () => {
    const testMapper = createMapper().field('name', { type: 'number' });

    const expected = { name: NaN };
    const actual = testMapper.map({ name: 'no number' });
    expect(actual).toEqual(expected);
  });

  test('should coerce 0 to false', () => {
    const testMapper = createMapper().field('name', { type: 'boolean' });

    const expected = { name: false };
    const actual = testMapper.map({ name: 0 });
    expect(actual).toEqual(expected);
  });

  test("should coerce '' to false", () => {
    const testMapper = createMapper().field('name', { type: 'boolean' });

    const expected = { name: false };
    const actual = testMapper.map({ name: '' });
    expect(actual).toEqual(expected);
  });

  test('should coerce non zero values to true', () => {
    const testMapper = createMapper().field('name', { type: 'boolean' });

    const expected = { name: true };
    const actual = testMapper.map({ name: -1 });
    expect(actual).toEqual(expected);
  });

  test('should coerce non empty strings to true', () => {
    const testMapper = createMapper().field('name', { type: 'boolean' });

    const expected = { name: true };
    const actual = testMapper.map({ name: 'false' });
    expect(actual).toEqual(expected);
  });
});
