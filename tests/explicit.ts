import { createExplicitMapper } from '../src/tsmapper';

interface Test {
  name?: string;
}

interface ChildTest {
  test?: string;
  other: string;
}
interface NestedTest {
  name?: string;
  child: ChildTest;
}

describe('optional', () => {
  test('value can be optional', () => {
    const testMapper = createExplicitMapper<Test>().field('name', { optional: true, type: 'string' });

    const expected: Test = {};

    const actual = testMapper.map({ unknown: 'test' });
    expect(actual).toEqual(expected);
  });
});

describe('nested', () => {
  test('value can be optional', () => {
    const childMapper = createExplicitMapper<ChildTest>()
      .field('test', { optional: true, type: 'string' })
      .field('other', { type: 'string' });

    const testMapper = createExplicitMapper<NestedTest>()
      .field('name', { optional: true, type: 'string' })
      .field('child', { mapper: childMapper });

    const expected: NestedTest = {
      child: { other: 'some' },
    };

    const actual = testMapper.map({ unknown: 'test', child: { other: 'some' } });
    expect(actual).toEqual(expected);
  });
});
