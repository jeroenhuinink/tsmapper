import { createMapper } from '../../src';
/* eslint-disable max-classes-per-file */

import { DateTime } from 'luxon';

test('custom date map', () => {
  const userMapper = createMapper('UserMapper', {
    map: {
      isodate: (val: unknown) => {
        const d = DateTime.fromISO(String(val), { zone: 'Europe/Amsterdam' });
        if (d.invalidReason) {
          throw new Error(`${d.invalidReason}, ${d.invalidExplanation}`);
        }
        return d.setZone('utc').toJSON();
      },
    },
  })
    .field('id', { type: 'string' })
    .field('createdAt', { type: 'isodate' });
  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    id: 'abc',
    createdAt: '2020-01-01',
  });

  const expected: User = { id: 'abc', createdAt: '2019-12-31T23:00:00.000Z' };

  expect(actual).toEqual(expected);
});
