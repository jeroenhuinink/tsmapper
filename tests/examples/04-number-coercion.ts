import { createMapper } from '../../src';

test('number coercion', () => {
  const userMapper = createMapper('UserMapper').field('id', { type: 'number' });

  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    id: '12',
  });

  const expected: User = {
    id: 12,
  };

  expect(actual).toEqual(expected);
});
