import { createMapper } from '../../src';

describe('date coercion', () => {
  test('date example', () => {
    const userMapper = createMapper('UserMapper').field('dateOfBirth', { type: 'date' });

    type User = ReturnType<typeof userMapper.map>;

    const actual = userMapper.map({
      dateOfBirth: '1972-07-20T00:00:00+01:00',
    });

    const expected: User = { dateOfBirth: new Date('1972-07-19T23:00:00.000Z') };

    expect(actual).toEqual(expected);
  });

  test('isodate example', () => {
    const userMapper = createMapper('UserMapper').field('dateOfBirth', { type: 'isodate' });

    type User = ReturnType<typeof userMapper.map>;

    const actual = userMapper.map({
      dateOfBirth: '1972-07-20T00:00:00+08:00',
    });

    const expected: User = { dateOfBirth: '1972-07-19T16:00:00.000Z' };

    expect(actual).toEqual(expected);
  });
});
