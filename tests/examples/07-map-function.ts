import { createMapper } from '../../src';

test('modifier', () => {
  const userMapper = createMapper('UserMapper').field('id', {
    map: (val: unknown) => (val as string).toUpperCase(),
  });

  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    id: 'abc',
  });

  const expected: User = { id: 'ABC' };

  expect(actual).toEqual(expected);
});
