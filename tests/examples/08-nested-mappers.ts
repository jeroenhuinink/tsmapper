import { createMapper } from '../../src';

test('nested mapper example', () => {
  const addressMapper = createMapper('AddressMapper')
    .field('street', { key: 'addressLine1', type: 'string' })
    .field('city', { key: 'addressLine2', type: 'string' })
    .field('country', { key: 'addressLine3', default: 'Nederland' });

  const userMapper = createMapper('UserMapper')
    .field('id', { key: 'userId', type: 'number' })
    .field('givenName', { type: 'string' })
    .field('familyName', { type: 'string' })
    .field('email', { type: 'string' })
    .field('visitingAddress', { key: 'address', mapper: addressMapper })
    .field('isAdmin', { type: 'boolean' });

  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    userId: '12',
    givenName: 'Jeroen',
    familyName: 'Huinink',
    email: 'jeroen.huinink@example.com',
    isAdmin: false,
    address: {
      addressLine1: 'Arthur van Schendelstraat 500',
      addressLine2: 'Utrecht',
    },
  });

  const expected: User = {
    id: 12,
    givenName: 'Jeroen',
    familyName: 'Huinink',
    email: 'jeroen.huinink@example.com',
    isAdmin: false,
    visitingAddress: {
      street: 'Arthur van Schendelstraat 500',
      city: 'Utrecht',
      country: 'Nederland',
    },
  };

  expect(actual).toEqual(expected);
});
