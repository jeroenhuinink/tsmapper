import { createMapper } from '../../src';

test('basic example', () => {
  const userMapper = createMapper('UserMapper')
    .field('id', { key: 'userId', type: 'number' })
    .field('givenName', { type: 'string' })
    .field('familyName', { type: 'string' })
    .field('email', { type: 'string' })
    .field('isAdmin', { type: 'boolean' });

  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    userId: '12',
    givenName: 'Jeroen',
    familyName: 'Huinink',
    email: 'jeroen.huinink@example.com',
    isAdmin: false,
  });

  const expected: User = {
    id: 12,
    givenName: 'Jeroen',
    familyName: 'Huinink',
    email: 'jeroen.huinink@example.com',
    isAdmin: false,
  };

  expect(actual).toEqual(expected);
});
