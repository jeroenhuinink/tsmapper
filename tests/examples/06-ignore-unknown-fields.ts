import { createMapper } from '../../src';

test('ignore unknown fields', () => {
  const userMapper = createMapper('UserMapper').field('id', { type: 'number' });

  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    id: '12',
    name: 'Jeroen Huinink',
  });

  const expected: User = {
    id: 12,
  };

  expect(actual).toEqual(expected);
});
