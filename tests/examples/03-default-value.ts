import { createMapper } from '../../src';

test('default value', () => {
  const coerceStringToNumber = createMapper('UserMapper').field('id', {
    default: 42,
  });

  type User = ReturnType<typeof coerceStringToNumber.map>;

  const actual = coerceStringToNumber.map({});

  const expected: User = { id: 42 };

  expect(actual).toEqual(expected);
});
