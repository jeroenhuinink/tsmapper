import { createExplicitMapper, createMapper } from '../../src';

interface Address {
  street: string;
  city: string;
  country: string;
}

interface User {
  id: number;
  givenName: string;
  familyName: string;
  email: string;
  addresses: Address[];
  isAdmin: boolean;
}

test('explicit nested mapper example', () => {
  const addressMapper = createExplicitMapper<Address>('AddressMapper')
    .field('street', { key: 'addressLine1', type: 'string' })
    .field('city', { key: 'addressLine2', type: 'string' })
    .field('country', { key: 'addressLine3', default: 'Nederland' });

  const userMapper = createExplicitMapper<User>('UserMapper')
    .field('id', { key: 'userId', type: 'number' })
    .field('givenName', { type: 'string' })
    .field('familyName', { type: 'string' })
    .field('email', { type: 'string' })
    .field('addresses', { key: 'address', default: [], itemType: { mapper: addressMapper } })
    .field('isAdmin', { type: 'boolean' });

  const actual = userMapper.map({
    userId: '12',
    givenName: 'Jeroen',
    familyName: 'Huinink',
    email: 'jeroen.huinink@example.com',
    isAdmin: false,
    address: [
      {
        addressLine1: 'Arthur van Schendelstraat 500',
        addressLine2: 'Utrecht',
      },
      {
        addressLine1: 'Arlandaweg 92',
        addressLine2: 'Amsterdam',
      },
    ],
  });

  const expected: User = {
    id: 12,
    givenName: 'Jeroen',
    familyName: 'Huinink',
    email: 'jeroen.huinink@example.com',
    isAdmin: false,
    addresses: [
      {
        street: 'Arthur van Schendelstraat 500',
        city: 'Utrecht',
        country: 'Nederland',
      },
      {
        street: 'Arlandaweg 92',
        city: 'Amsterdam',
        country: 'Nederland',
      },
    ],
  };

  expect(actual).toEqual(expected);
});

test('implicit nested mapper example', () => {
  const addressMapper = createMapper('AddressMapper')
    .field('street', { key: 'addressLine1', type: 'string' })
    .field('city', { key: 'addressLine2', type: 'string' })
    .field('country', { key: 'addressLine3', default: 'Nederland' });

  const userMapper = createMapper('UserMapper')
    .field('id', { key: 'userId', type: 'number' })
    .field('givenName', { type: 'string' })
    .field('familyName', { type: 'string' })
    .field('email', { type: 'string' })
    .field('addresses', { key: 'address', default: [], itemType: { mapper: addressMapper } })
    .field('isAdmin', { type: 'boolean' });

  type DerivedUser = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    userId: '12',
    givenName: 'Jeroen',
    familyName: 'Huinink',
    email: 'jeroen.huinink@example.com',
    isAdmin: false,
    address: [
      {
        addressLine1: 'Arthur van Schendelstraat 500',
        addressLine2: 'Utrecht',
      },
      {
        addressLine1: 'Arlandaweg 92',
        addressLine2: 'Amsterdam',
      },
    ],
  });

  const expected: DerivedUser = {
    id: 12,
    givenName: 'Jeroen',
    familyName: 'Huinink',
    email: 'jeroen.huinink@example.com',
    isAdmin: false,
    addresses: [
      {
        street: 'Arthur van Schendelstraat 500',
        city: 'Utrecht',
        country: 'Nederland',
      },
      {
        street: 'Arlandaweg 92',
        city: 'Amsterdam',
        country: 'Nederland',
      },
    ],
  };

  expect(actual).toEqual(expected);
});
