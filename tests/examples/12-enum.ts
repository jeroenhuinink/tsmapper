import { createMapper } from '../../src';
/* eslint-disable max-classes-per-file */

test('lookup value from enum map', () => {
  const userMapper = createMapper('UserMapper').field('role', {
    enum: ['admin', 'manager', 'owner', 'user'] as const,
  });

  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    role: 'manager',
  });

  const expected: User = { role: 'manager' };

  expect(actual).toEqual(expected);
});
