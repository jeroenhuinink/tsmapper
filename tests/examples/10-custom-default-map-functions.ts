import { createMapper } from '../../src';

test('custom default map', () => {
  const userMapper = createMapper('UserMapper', {
    map: {
      boolean: (val: unknown) => val === 'Y' || val === 'Yes',
    },
  })
    .field('id', { type: 'string' })
    .field('isAdmin', { type: 'boolean' })
    .field('isActive', { type: 'boolean' });

  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    id: 'abc',
    isAdmin: 'N',
    isActive: 'Y',
  });

  const expected: User = { id: 'abc', isAdmin: false, isActive: true };

  expect(actual).toEqual(expected);
});
