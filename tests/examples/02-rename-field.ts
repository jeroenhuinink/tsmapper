import { createMapper } from '../../src';

test('rename field', () => {
  const userMapper = createMapper('UserMapper')
    .field('uniqueId', { type: 'number', key: 'unique-id' })
    .field('firstName', { type: 'string', key: 'given-name' })
    .field('lastName', { type: 'string', key: 'family-name' });
  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map({
    'unique-id': 12,
    'given-name': 'Jeroen',
    'family-name': 'Huinink',
  });

  const expected: User = {
    uniqueId: 12,
    firstName: 'Jeroen',
    lastName: 'Huinink',
  };

  expect(actual).toEqual(expected);
});
