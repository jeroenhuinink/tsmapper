import { createMapper } from '../../src';

export enum UserRole {
  Administrator = 'Administrator',
  User = 'User',
}

test('get values from enummapper in array', () => {
  const testJsonInput = {
    userCode: 'Jeroen',
    displayName: 'Jeroen Huinink',
    roles: ['Administrator', 'User'],
    languageISO: 'nl',
  };

  function userRoleMapper(role: any): UserRole {
    if (role in UserRole) {
      return UserRole[role] as UserRole;
    }
    throw new Error('unexpected role');
  }

  const userMapper = createMapper('UserMapper')
    .field('name', { type: 'string', key: 'userCode' })
    .field('displayName', { type: 'string', optional: true })
    .field('roles', { type: 'array', itemType: { type: 'enum', map: userRoleMapper } })
    .field('language', { type: 'string', key: 'languageISO', optional: true })
    .field('email', { type: 'string', optional: true })
    .field('updatedAt', { type: 'isodate', optional: true });

  type User = ReturnType<typeof userMapper.map>;

  const actual = userMapper.map(testJsonInput);

  const expected: User = {
    name: 'Jeroen',
    email: undefined,
    displayName: 'Jeroen Huinink',
    roles: [UserRole.Administrator, UserRole.User],
    language: 'nl',
    updatedAt: undefined,
  };

  expect(actual).toEqual(expected);
});
