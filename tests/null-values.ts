import { createMapper } from '../src/tsmapper';

describe('null values', () => {
  it('should use default value if field is null', () => {
    const testMapper = createMapper('a').field('test', { type: 'string', default: '' });
    const expected = { test: '' };
    const actual = testMapper.map({ test: null });
    expect(actual).toEqual(expected);
  });

  it('should skip if child element is null', () => {
    const childMapper = createMapper('b').field('test', { type: 'string' });
    const testMapper = createMapper('a').field('child', { mapper: childMapper, optional: true });
    const expected = {};
    const actual = testMapper.map({ child: null });
    expect(actual).toEqual(expected);
  });

  it('should skip if child element is null', () => {
    const childMapper = createMapper('b').field('test', { type: 'string' });
    const testMapper = createMapper('a').field('child', { mapper: childMapper, default: {} });
    const expected = { child: {} };
    const actual = testMapper.map({ child: null });
    expect(actual).toEqual(expected);
  });
});
