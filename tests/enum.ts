import { createMapper } from '../src';
/* eslint-disable max-classes-per-file */

describe('enum', () => {
  it('should lookup value from enum map', () => {
    const userMapper = createMapper('UserMapper').field('role', {
      enum: ['admin', 'manager', 'owner', 'user'] as const,
    });

    type User = ReturnType<typeof userMapper.map>;

    const actual = userMapper.map({
      role: 'manager',
    });

    const expected: User = { role: 'manager' };

    expect(actual).toEqual(expected);
  });

  it('should allow numeric lookup from enum map', () => {
    const userMapper = createMapper('UserMapper').field('role', {
      enum: ['admin', 'manager', 'owner', 'user'] as const,
    });

    type User = ReturnType<typeof userMapper.map>;

    const actual = userMapper.map({
      role: 1,
    });

    const expected: User = { role: 'manager' };

    expect(actual).toEqual(expected);
  });

  it('should use defaultValue from enum map if not present', () => {
    const userMapper = createMapper('UserMapper').field('role', {
      enum: ['admin', 'manager', 'owner', 'user'] as const,
      default: 'user',
    });

    type User = ReturnType<typeof userMapper.map>;

    const actual = userMapper.map({});

    const expected: User = { role: 'user' };

    expect(actual).toEqual(expected);
  });

  it('should throw if value not found in enum map', () => {
    const userMapper = createMapper('UserMapper').field('role', {
      enum: ['admin', 'manager', 'owner', 'user'] as const,
    });

    type User = ReturnType<typeof userMapper.map>;

    const actual = () => {
      userMapper.map({
        role: 'power-user',
      });
    };

    expect(actual).toThrowError(
      'UserMapper: Value "power-user" not allowed for property "role" in: {"role":"power-user"}',
    );
  });

  it('should throw if numeric value not found in enum map', () => {
    const userMapper = createMapper('UserMapper').field('role', {
      enum: ['admin', 'manager', 'owner', 'user'] as const,
    });

    type User = ReturnType<typeof userMapper.map>;

    const actual = () => {
      userMapper.map({
        role: 4,
      });
    };

    expect(actual).toThrowError('UserMapper: Value 4 not allowed for property "role" in: {"role":4}');
  });

  it('should throw if property not present', () => {
    const userMapper = createMapper('UserMapper').field('role', {
      enum: ['admin', 'manager', 'owner', 'user'] as const,
    });

    type User = ReturnType<typeof userMapper.map>;

    const actual = () => {
      userMapper.map({});
    };

    expect(actual).toThrowError('UserMapper: Property "role" not found in: {}');
  });
});
