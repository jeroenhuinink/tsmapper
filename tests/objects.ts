import { createMapper } from '../src';

describe('nested mapper', () => {
  describe('object', () => {
    it('should use mapper from field', () => {
      const aMapper = createMapper('a').field('value', { type: 'string' });
      const bMapper = createMapper('b').field('a', { key: 'c', mapper: aMapper });

      const actual = bMapper.map({ c: { value: 'test' } });
      const expected = { a: { value: 'test' } };

      expect(actual).toEqual(expected);
    });

    it('should allow optional subelement', () => {
      const aMapper = createMapper('a').field('value', { type: 'string' });
      const bMapper = createMapper('b')
        .field('a', { key: 'c', optional: true, mapper: aMapper })
        .field('d', { default: 'D' });

      const actual = bMapper.map({});
      const expected = { d: 'D' };

      expect(actual).toEqual(expected);
    });

    it('should throw if not present', () => {
      const aMapper = createMapper('ChildMapper').field('value', { type: 'string' });
      const bMapper = createMapper('TestMapper').field('a', { key: 'c', mapper: aMapper });

      const actual = () => bMapper.map({});

      expect(actual).toThrowError('TestMapper: Property "c" not found or undefined in: {}');
    });

    it('should throw on submapper error', () => {
      const aMapper = createMapper('ChildMapper').field('value', { type: 'string' });
      const bMapper = createMapper('TestMapper').field('a', { key: 'c', mapper: aMapper });

      const actual = () => bMapper.map({ c: {} });

      expect(actual).toThrowError(
        'TestMapper: Mapper failed for property c with error "ChildMapper: Property "value" not found in: {}" in: {"c":{}}',
      );
    });
  });
});
