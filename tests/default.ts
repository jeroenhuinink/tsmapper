import { createMapper } from '../src/tsmapper';

describe('default', () => {
  test('should use defaults if specified', () => {
    const testMapper = createMapper().field('name', { default: 'sentastic' });
    const expected = { name: 'sentastic' };
    const actual = testMapper.map({});
    expect(actual).toEqual(expected);
  });

  test('should use empty default', () => {
    const testMapper = createMapper().field('name', { default: '' });

    const expected = { name: '' };
    const actual = testMapper.map({ unknown: 'test' });
    expect(actual).toEqual(expected);
  });
});
