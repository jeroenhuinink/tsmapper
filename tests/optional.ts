import { createMapper } from '../src/tsmapper';

describe('optional', () => {
  test('value can be optional', () => {
    const testMapper = createMapper().field('name', { optional: true, type: 'string' });

    const expected = {};
    const actual = testMapper.map({ unknown: 'test' });
    expect(actual).toEqual(expected);
  });
});
