import { createMapper } from '../src/tsmapper';

describe('mapper', () => {
  test('should return known attributes', () => {
    const testMapper = createMapper().field('name', { type: 'string' });

    const expected = { name: 'sentastic' };
    const actual = testMapper.map({ name: 'sentastic' });
    expect(actual).toEqual(expected);
  });

  test('should skip unknown attributes', () => {
    const testMapper = createMapper().field('name', { type: 'string' });

    const expected = { name: 'sentastic' };
    const actual = testMapper.map({ name: 'sentastic', unknown: 'sentastic' });
    expect(actual).toEqual(expected);
  });

  test('should fail on missing attributes', () => {
    const testMapper = createMapper().field('name', { type: 'string' });

    const func = () => {
      const actual = testMapper.map({ unknown: 'sentastic' });
    };
    expect(func).toThrow();
  });
});
