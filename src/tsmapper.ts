import {
  MapFunctions,
  MapperField,
  Options,
  Source,
  ValueOptions,
  ArrayOptions,
  EnumOptions,
  ExplicitMapper,
  ImplicitMapper,
} from './types';

/**
 * Default set of functions that are used to map values from the source to the target.
 */
const defaultMapFunctions: MapFunctions = {
  string: (val: unknown) => String(val),
  number: (val: unknown) => Number(val),
  boolean: (val: unknown) => !!val,
  date: (val: unknown) => {
    return new Date(String(val));
  },
  isodate: (val: unknown) => {
    return new Date(String(val)).toISOString();
  },
  symbol: (val: unknown) => Symbol(val as any),
  undefined: (val: unknown) => String(val),
  object: (val: unknown) => JSON.stringify(val),
  function: (val: unknown) => val,
};

/**
 * Errors throw by the Mapper
 */
class MapperError extends Error {
  source: unknown;

  constructor(message: string, source: unknown) {
    super(`${message} in: ${JSON.stringify(source)}`);
    this.source = source;
  }
}

/**
 * The Mapper implementation
 */
class MapperInstance<T> {
  private fields: MapperField[];
  private mapFunctions: MapFunctions;
  private name: string;

  constructor(name: string, options?: { map?: Partial<MapFunctions> }) {
    const map = options?.map || {};
    this.fields = [];
    this.name = name;
    this.mapFunctions = { ...defaultMapFunctions, ...map };
  }

  field(name: string, options: Options): MapperInstance<T> {
    const mapperField: MapperField = {
      ...options,
      /* Fields are mandatory (non-optional) by default */
      optional: options.optional || false,
      /* If no key is present, the name is used as the key */
      key: options.key || name,
      name,
    };

    /* If there is a enum option it is a type object */
    if ('enum' in options) {
      mapperField.type = 'enum';
      (mapperField as EnumOptions).enum = options.enum;
    }

    /* If there is a mapper option it is a type object */
    if ('mapper' in options) {
      mapperField.type = 'object';
    }

    /* If there is an itemType option it is of type array */
    if ('itemType' in options) {
      mapperField.type = 'array';
      if (!options.itemType.type) {
        (mapperField as ArrayOptions).itemType.type = 'object';
      }
    }

    this.fields.push(mapperField);

    return this;
  }

  /**
   *
   * The function that performs the actual map from the source to the target.
   *
   * @param source
   * @returns target
   */
  public map(source: Source) {
    return this.fields.reduce((result, field) => {
      const { name, key, default: defaultValue, optional } = field;
      switch (field.type) {
        case 'enum':
          if (!(key in source)) {
            if (!defaultValue) {
              throw new MapperError(`${this.name}: Property "${key}" not found`, source);
            }
            return { ...result, [name]: defaultValue };
          }

          const enumArray = (field as EnumOptions).enum;
          const enumValue = source[key];

          if (typeof enumValue === 'number') {
            if (!(enumValue in enumArray)) {
              throw new MapperError(`${this.name}: Value ${enumValue} not allowed for property "${key}"`, source);
            }
            return { ...result, [name]: enumArray[enumValue] };
          }

          if (!enumArray.includes(String(enumValue))) {
            throw new MapperError(`${this.name}: Value "${enumValue}" not allowed for property "${key}"`, source);
          }

          return { ...result, [name]: String(enumValue) };

        case 'array':
          if (!(key in source)) {
            if (defaultValue) {
              return {
                ...result,
                [name]: defaultValue,
              };
            }

            if (!optional) {
              throw new MapperError(`${this.name}: Property "${key}" not found`, source);
            }

            return result;
          }

          switch (field.itemType.type) {
            case 'enum':
              const itemMap = field.itemType.map;

              return {
                ...result,
                [name]: (source[key] as Source[]).map((item) => itemMap(item)),
              };

            default:
              const itemMapper = field.itemType.mapper;

              return {
                ...result,
                [name]: (source[key] as Source[]).map((item) => itemMapper.map(item)),
              };
          }

        case 'object':
          // eslint-disable-next-line no-case-declarations
          const { mapper } = field;
          if (!(key in source) || !source[key]) {
            if (defaultValue) {
              return {
                ...result,
                [name]: defaultValue,
              };
            }
            if (!optional) {
              throw new MapperError(`${this.name}: Property "${key}" not found or undefined`, source);
            }
            return result;
          }

          try {
            return {
              ...result,
              [name]: mapper.map(source[key] as Source),
            };
          } catch (error) {
            throw new MapperError(
              `${this.name}: Mapper failed for property ${key} with error "${error.message}"`,
              source,
            );
          }
        default:
          const { type } = field as MapperField & ValueOptions;

          const value = source[key];

          const map = (field as ValueOptions).map || this.mapFunctions[type || typeof defaultValue];

          if (value === undefined || value === null) {
            if (defaultValue !== undefined) {
              return {
                ...result,
                [name]: map(defaultValue, source),
              };
            }
            if (optional) {
              return result;
            }

            throw new MapperError(`${this.name}: Property "${key}" not found`, source);
          }

          return { ...result, [name]: map(value, source) };
      }
    }, {} as T);
  }
}

/**
 * The options that can be passed when creating a mapper.
 */
type MapperOptions = {
  /**
   * Partial set of functions to override the default value mapping.
   */
  map?: Partial<MapFunctions>;
};

/**
 *
 * @param name Specify a name for your mapper. This name will be used in error messages.
 * @param options Mapper options
 * @returns
 */
export function createMapper(name = 'Mapper', options?: MapperOptions) {
  return new MapperInstance(name, options) as ImplicitMapper;
}

export function createExplicitMapper<T extends {}>(name = 'Mapper', options?: MapperOptions) {
  return new MapperInstance<T>(name, options) as ExplicitMapper<T>;
}
