# Tsmapper

Library to map JSON (API responses) to typed objects for Typescript.

I like types, because when you have type information your IDE and other tools can help you avoid mistakes. Because I like types, I also like to use Typescript for frontend and backend development. A lot of what we do as either frontend and backend developers is interpret bodies sent as as part of (REST) API requests or responses. Because REST is in itself untyped and the request and response body for an HTTP request can contain arbitrary data, I like to not make any assumptions about the types of data and if data comes in an unexpected format you should fail fast to avoid hard to track errors down the line.

This library helps you interpret (map) API responses and return a guaranteed typescript (interface) type.

This library took inspiration from [mappet](https://github.com/MichalZalecki/mappet) and the [yargs type definitions](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/yargs/index.d.ts).

## Features

- Fields can be renamed.
- Ensures presence of all fields. Fields can be declared optional.
- Coerce field value to expected type.
- Strip all fields in JSON that are not recognized.
- Fluent interface to define the mapper.
- Derive type definition from mapper definition. No need for a seperate interface defintion.
- Can specify default values for missing fields. Specifing a default value `null` makes the field optional.
- Can specify modifier functions to modify values or coerce field values using your own functions. The modifier function can access all fields in the input.
- Can specify include functions to dynamically determine if field must be included, based on all fields in the input.
  
## Installation

```sh
npm install @jeroenhuinink/tsmapper
```

or

```sh
yarn add @jeroenhuinink/tsmapper
```

## Usage

### Basic Usage

You can call the `createMapper` function to create a mapper. The function has an optional string parameter for the mapper name. This will be included in error messages. The create mapper has a `mapper.map(source)` function that will map a source JSON to a target as defined by the fields added to the mapper..

You can use `.field(name, options)` to add field declarations to the mapper. The name is the field name that is should have in the target. In the options you can specify a key for the name that the field has in the source. You can also specify default values and modifier functions. If the type can not be derived from the default value or the modifier function you can also specify a type. Otherwise a type of `unknown` will be drived.

```ts
import {createMapper} from '@jeroenhuinink/tsmapper';

const userMapper = createMapper('UserMapper')
  .field('id', { key: 'userId', type: 'number' })
  .field('givenName', { type: 'string' })
  .field('familyName', { type: 'string' })
  .field('email', { type: 'string' })
  .field('isAdmin', { type: 'boolean' });

const user = userMapper.map({
  userId: '12',
  givenName: 'Jeroen',
  familyName: 'Huinink',
  email: 'jeroen.huinink@example.com',
  isAdmin: false,
});
```

You can use `type User = ReturnType<typeof userMapper.map>` to get the type of the target.

More usage examples can be found in the [examples subdirectory of the tests directory](https://gitlab.com/jeroenhuinink/tsmapper/-/tree/main/tests/examples).
